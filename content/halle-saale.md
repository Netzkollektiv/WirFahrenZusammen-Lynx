+++
title = '#WirFahrenZusammen Halle (Saale)'
date = 2023-11-20T14:00:29+01:00
draft = false

image = "../img/wirfahrenzusammen.jpg"
links = [
  # keep this here
  { link = { href = "/", text = "#WirFahrenZusammen" } },

  { email = { href = "mailto:wirfahrenzusammen@fff-halle.de", text = "Schreib uns eine Mail" } },
  # { link = { href = "", text = "" } },
  # { amazon = { href = "", text = "" } },
  # { apple = { href = "", text = "" } },
  # { codepen = { href = "", text = "" } },
  # { dev = { href = "", text = "" } },
  # { discord = { href = "", text = "" } },
  # { dribbble = { href = "", text = "" } },
  # { facebook = { href = "", text = "" } },
  # { flickr = { href = "", text = "" } },
  # { foursquare = { href = "", text = "" } },
  # { github = { href = "", text = "" } },
  # { gitlab = { href = "", text = "" } },
  # { google = { href = "", text = "" } },
  # { instagram = { href = "", text = "" } },
  # { keybase = { href = "", text = "" } },
  # { kickstarter = { href = "", text = "" } },
  # { lastfm = { href = "", text = "" } },
  # { linkedin = { href = "", text = "" } },
  # { mastodon = { href = "", text = "" } },
  # { medium = { href = "", text = "" } },
  # { microsoft = { href = "", text = "" } },
  # { patreon = { href = "", text = "" } },
  # { pinterest = { href = "", text = "" } },
  # { reddit = { href = "", text = "" } },
  # { slack = { href = "", text = "" } },
  # { snapchat = { href = "", text = "" } },
  # { soundcloud = { href = "", text = "" } },
  # { stack-exchange = { href = "", text = "" } },
  # { stack-overflow = { href = "", text = "" } },
  # { steam = { href = "", text = "" } },
  { telegram = { href = "https://t.me/WirFahrenZusammenHAL", text = "Infos über Telegram" } },
  # { tiktok = { href = "", text = "" } },
  # { tumblr = { href = "", text = "" } },
  # { twitch = { href = "", text = "" } },
  # { twitter = { href = "", text = "" } },
  { whatsapp = { href = "https://whatsapp.com/channel/0029VaDefRNGE56jmQ5iKm3D", text = "Infos über WhatsApp" } },
  # { youtube = { href = "", text = "" } },
  { fff = { href = "https://fff-halle.de/wirfahrenzusammen", text = "Fridays For Future Halle" } },
  { verdi = { href = "https://sachsen-anhalt-sued.verdi.de/branchen-und-berufe/verkehr/betriebsgruppe-havag", text = "ver.di Betriebsgruppe HAVAG" } },
]
+++
