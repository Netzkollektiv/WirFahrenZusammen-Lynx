# WirFahrenZusammen-Lynx

This is a hugo project and modification of the https://github.com/jpanther/lynx theme for the [#WirFahrenZusammen](https://wir-fahren-zusammen.de)-campaign of [Fridays For Future Germany](https://fridaysforfuture.de) and [ver.di](https://verdi.de).

Example sites of this project can be found at https://wir-fahren-zusammen.org or https://wir-fahren-zusammen.org/halle-saale.

This project is not affiliated with any of the beforementioned groups but an individual community project to support the cause. Therefore we're greatful for any contributions to this project or the Lynx theme.

## Getting Started
1. Make sure you got hugo installed. Everything you need is documented at https://gohugo.io/documentation/.
2. Clone the project and build it with `hugo server -DF --disableFastRender` in your terminal. The website is now served at an adress displayed by the hugo server.

## How to contribute your local info
1. After you finished the Getting Started section and you're able to see the site in your browser you can add a local site by creating them with the command `hugo new <$city>.md`. Edit your links in the hugo Front Matter with TOML formatting.
2. Build your site with the `hugo` command and extract the site from the `/public` directory.
3. Create a branch with the name of your city and a corresponding pull request. The request will be reviewed asap. 
