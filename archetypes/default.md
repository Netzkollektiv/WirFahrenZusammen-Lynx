+++
title = '#WirFahrenZusammen {{ replace .File.ContentBaseName "-" " " | title }}'
date = {{ .Date }}
draft = true


links = [
  # keep this here
  { link = { href = "/", text = "#WirFahrenZusammen" } },

  # { email = { href = "mailto:hello@your_domain.com", text = "" } },
  # { link = { href = "", text = "" } },
  # { amazon = { href = "", text = "" } },
  # { apple = { href = "", text = "" } },
  # { codepen = { href = "", text = "" } },
  # { dev = { href = "", text = "" } },
  # { discord = { href = "", text = "" } },
  # { dribbble = { href = "", text = "" } },
  # { facebook = { href = "", text = "" } },
  # { flickr = { href = "", text = "" } },
  # { foursquare = { href = "", text = "" } },
  # { github = { href = "", text = "" } },
  # { gitlab = { href = "", text = "" } },
  # { google = { href = "", text = "" } },
  # { instagram = { href = "", text = "" } },
  # { keybase = { href = "", text = "" } },
  # { kickstarter = { href = "", text = "" } },
  # { lastfm = { href = "", text = "" } },
  # { linkedin = { href = "", text = "" } },
  # { mastodon = { href = "", text = "" } },
  # { medium = { href = "", text = "" } },
  # { microsoft = { href = "", text = "" } },
  # { patreon = { href = "", text = "" } },
  # { pinterest = { href = "", text = "" } },
  # { reddit = { href = "", text = "" } },
  # { slack = { href = "", text = "" } },
  # { snapchat = { href = "", text = "" } },
  # { soundcloud = { href = "", text = "" } },
  # { stack-exchange = { href = "", text = "" } },
  # { stack-overflow = { href = "", text = "" } },
  # { steam = { href = "", text = "" } },
  # { telegram = { href = "", text = "" } },
  # { tiktok = { href = "", text = "" } },
  # { tumblr = { href = "", text = "" } },
  # { twitch = { href = "", text = "" } },
  # { twitter = { href = "", text = "" } },
  # { whatsapp = { href = "", text = "" } },
  # { youtube = { href = "", text = "" } },
]
+++
